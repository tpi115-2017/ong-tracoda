<?php

namespace Tracoda\PlantillaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityManager;
//use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;

class DocumentoType extends AbstractType
{

    /** @var \Doctrine\ORM\EntityManager */
    private $em;

    /**
     * Constructor
     *
     * @param EntityManager $doctrine
     */
    public function __construct(EntityManager $doctrine)
    {
        $this->em = $doctrine;
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('tituloDocumento','text',array(
                'label' => 'Título',
                 'attr' =>array(
                   'class' => 'form-control',
                   'placeholder' => 'Título de documento',
                   'maxlength' => '255')))

        ->add('idDataset','entity',array(
                'label' => 'Dataset',
                'class' => 'TracodaModeloBundle:Dataset',
                'choices' => $this->getDatasets(),
                'attr' =>array(
                  'class' => 'form-control')))

        ->add('documento', 'file',
            array(
              'label' => 'Documento',
              'data_class' => null,
              'attr' => array(
                'class' => 'form-control',
                'placeholder' => 'URL')))

        ->add('descripcionDocumento','text',array(
          'attr'=>array(
            'placeholder'=>'Descripción del documeto'
          )));

    }

    private function getDatasets()
    {
        if ($this->em != null)
            $children = $this->em->getRepository('TracodaModeloBundle:Dataset')->findAll();
        else
            $children = array();
        return $children;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tracoda\ModeloBundle\Entity\Documento'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'tracoda_modelobundle_documento';
    }


}
