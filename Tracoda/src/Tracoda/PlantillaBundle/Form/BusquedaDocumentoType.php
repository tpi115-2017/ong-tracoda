<?php

namespace Tracoda\PlantillaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/*Se importan librerias para la validación de los campos*/

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;

class BusquedaDocumentoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      /*Se establecen los campos para el formulario de opnion */
        $builder
            ->setAction('resultado-busqueda')
            ->add('busco', 'text', array(
                'attr' => array(
                    'placeholder' => 'Estoy buscando....'
                )
            ))
            ->add('buscar', 'submit', array('label' => 'Buscar'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
      /*A los campos defininos del formulario de busqueda se les establecen sus validaciones*/
        $collectionConstraint = new Collection(array(
            'busco' => array(
                new NotBlank(array('message' => 'Ingrese algún término de busqueda.'))
            )
        ));

        $resolver->setDefaults(array(
            'constraints' => $collectionConstraint
        ));
    }

    public function getName()
    {
        return 'Buscando';
    }
}
