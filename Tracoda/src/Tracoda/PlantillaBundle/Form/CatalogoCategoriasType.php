<?php

namespace Tracoda\PlantillaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class CatalogoCategoriasType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombreCategoria', TextType::class, array('label' => 'Categoría', 'attr' => array('class' => 'form-control', 'placeholder' => 'Categoría')))->
        add('imagenCategoria', 'file', 
            array('label' => 'Imagen', 'data_class' => null, 'attr' => array('class' => 'form-control', 'placeholder' => 'URL')));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tracoda\ModeloBundle\Entity\CatalogoCategorias'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'tracoda_modelobundle_catalogocategorias';
    }


}
