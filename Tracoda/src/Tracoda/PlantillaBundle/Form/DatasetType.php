<?php

namespace Tracoda\PlantillaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Doctrine\ORM\EntityManager;

class DatasetType extends AbstractType
{
    /** @var \Doctrine\ORM\EntityManager */
    private $em;

    /**
     * Constructor
     * 
     * @param EntityManager $doctrine
     */
    public function __construct(EntityManager $doctrine)
    {
        $this->em = $doctrine;
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titulo', TextType::class,
            array('label' => 'Título', 'attr' => 
                array('class' => 'form-control', 'placeholder' => 'Título de dataset', 'maxlength' => '100')))->
        //add('fechaSubida')->
        add('descripcion', TextType::class,
            array('label' => 'Descripción', 'attr' => 
                array('class' => 'form-control', 'placeholder' => 'Descripción', 'maxlength' => '255')))->
        add('fuenteDatos', TextType::class,
            array('label' => 'Fuente', 'attr' => 
                array('class' => 'form-control', 'placeholder' => 'Fuente de obtención', 'maxlength' => '255')))->
        //add('numVisitas')->
        add('publicado', CheckboxType::class,
            array('label' => 'Publicar', ))->
        add('idLicencia',
            'entity',
            //ChoiceType::class,
            array('label' => 'Licencia', 
                'class' => 'TracodaModeloBundle:Licencia',
                'choices' => $this->getLicencias(),
                'attr' => 
                array('class' => 'form-control')))->
        add('idCategoria',
            'entity',
            //ChoiceType::class,
            array('label' => 'Categoría', 
                'class' => 'TracodaModeloBundle:CatalogoCategorias',
                'choices' => $this->getCategorias(),
                'attr' => 
                array('class' => 'form-control')));
        //add('idUsuario');
    }

    private function getCategorias()
    {
        if ($this->em != null)
            $children = $this->em->getRepository('TracodaModeloBundle:CatalogoCategorias')->findAll();
        else
            $children = array();
        return $children;
    }

    private function getLicencias()
    {
        if ($this->em != null)
            $children = $this->em->getRepository('TracodaModeloBundle:Licencia')->findAll();
        else
            $children = array();
        return $children;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tracoda\ModeloBundle\Entity\Dataset'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'tracoda_modelobundle_dataset';
    }


}
