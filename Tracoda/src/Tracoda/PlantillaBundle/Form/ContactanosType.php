<?php

namespace Tracoda\PlantillaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/*Se importan librerias para la validación de los campos*/
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;

class ContactanosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      /*Se establecen los campos para el formulario de opnion */
        $builder
            ->add('nombre', 'text', array(
                'attr' => array(
                    'placeholder' => 'Ingrese su nombre'
                )
            ))
            ->add('email', 'email', array(
                'attr' => array(
                    'placeholder' => 'Email de contacto'
                )
            ))
            ->add('asunto', 'text', array(
                'attr' => array(
                    'placeholder' => 'Asunto'
                )
            ))
            ->add('mensaje', 'textarea', array(
                'attr' => array(
                    'cols' => 90,
                    'rows' => 10,
                    'placeholder' => 'Ingrese su mensaje'
                )
            ))
            ->add('save', 'submit', array('label' => 'Enviar'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
      /*A los campos defininos del formulario de opinion se les establecen sus validaciones*/
        $collectionConstraint = new Collection(array(
            'nombre' => array(
                new NotBlank(array('message' => 'El nombre no puede estar vacío.')),
                new Length(array('min' => 3))
            ),
            'email' => array(
                new NotBlank(array('message' => 'El email no puede estar vacío.')),
                new Email(array('message' => 'Email inválido debe ser en formato nombre@dominio.com '))
            ),
            'asunto' => array(
                new NotBlank(array('message' => 'El asunto no puede estar vacío.')),
                new Length(array('min' => 3, 'minMessage' => 'Debes ingresar al menos {{ limit }} caracteres.',))
            ),
            'mensaje' => array(
                new NotBlank(array('message' => 'El mensaje no puede estar vacío.')),
                new Length(array('min' => 5,'minMessage' => 'Debes ingresar al menos {{ limit }} caracteres.'))
            )
        ));

        $resolver->setDefaults(array(
            'constraints' => $collectionConstraint
        ));
    }

    public function getName()
    {
        return 'opinion';
    }
}
