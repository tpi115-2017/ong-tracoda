<?php

namespace Tracoda\PlantillaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SeguridadUsuarioType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nombre','text',array(
          'attr'=> array(
            'placeholder' => 'Nombre de usuario'
          )
        ))
        ->add('password','password',array(
          'attr' => array(
            'placeholder' =>  'Contraseña'
          )
        ))
        ->add('correo','email',array(
          'attr' => array(
            'placeholder' => 'correo@dominio.com'
          )
        ))
        ->add('facebook','url',array(
          'attr' => array(
            'placeholder' => 'https://facebook.com/usuario'
          )
        ))
        ->add('twitter','url',array(
          'attr' => array(
            'placeholder' => 'https://twitter.com/usuario'
          )
        ))
        ->add('habilitado')
        ->add('idRol');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tracoda\ModeloBundle\Entity\SeguridadUsuario'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'tracoda_modelobundle_seguridadusuario';
    }


}
