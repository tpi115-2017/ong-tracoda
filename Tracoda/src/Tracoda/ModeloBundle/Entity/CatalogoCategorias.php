<?php

namespace Tracoda\ModeloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CatalogoCategorias
 *
 * @ORM\Table(name="catalogo_categorias", uniqueConstraints={@ORM\UniqueConstraint(name="nombre_categoria", columns={"nombre_categoria"})})
 * @ORM\Entity
 */
class CatalogoCategorias
{
    /**
     * @var string
     *
     * @ORM\Column(name="nombre_categoria", type="string", length=200, nullable=false)
     */
    private $nombreCategoria;

    /**
     * @var string
     * *@Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "El maximo permitido es 5MB.",
     *     mimeTypesMessage = "únicamente se permiten archivos con formato de imagen."
     *)
     * @ORM\Column(name="imagen_categoria", type="string", length=100, nullable=true)
     */
    private $imagenCategoria;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreCategoria
     *
     * @param string $nombreCategoria
     * @return CatalogoCategorias
     */
    public function setNombreCategoria($nombreCategoria)
    {
        $this->nombreCategoria = $nombreCategoria;

        return $this;
    }

    /**
     * Get nombreCategoria
     *
     * @return string
     */
    public function getNombreCategoria()
    {
        return $this->nombreCategoria;
    }

    /**
     * Set imagenCategoria
     *
     * @param string $imagenCategoria
     * @return CatalogoCategorias
     */
    public function setImagenCategoria($imagenCategoria)
    {
        $this->imagenCategoria = $imagenCategoria;

        return $this;
    }

    /**
     * Get nombreCategoria
     *
     * @return string
     */
    public function getImagenCategoria()
    {
        return $this->imagenCategoria;
    }

    public function __toString()
    {
        return $this->nombreCategoria;//getNombreCategoria();
    }
}
