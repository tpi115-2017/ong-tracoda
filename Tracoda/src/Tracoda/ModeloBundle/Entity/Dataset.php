<?php

namespace Tracoda\ModeloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dataset
 *
 * @ORM\Table(name="dataset", indexes={@ORM\Index(name="FK_DatasetUsuario", columns={"id_usuario"}), @ORM\Index(name="FK_DatasetCategoria", columns={"id_categoria"}), @ORM\Index(name="FK_DatasetLicencia", columns={"id_licencia"})})
 * @ORM\Entity
 */
class Dataset
{
    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=100, nullable=false)
     */
    private $titulo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_subida", type="datetime", nullable=false)
     */
    private $fechaSubida;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="fuente_datos", type="string", length=255, nullable=false)
     */
    private $fuenteDatos;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_visitas", type="integer", nullable=false)
     */
    private $numVisitas;

    /**
     * @var boolean
     *
     * @ORM\Column(name="publicado", type="boolean", nullable=false)
     */
    private $publicado;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_dataset", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idDataset;

    /**
     * @var \Tracoda\ModeloBundle\Entity\Licencia
     *
     * @ORM\ManyToOne(targetEntity="Tracoda\ModeloBundle\Entity\Licencia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_licencia", referencedColumnName="id_licencia")
     * })
     */
    private $idLicencia;

    /**
     * @var \Tracoda\ModeloBundle\Entity\CatalogoCategorias
     *
     * @ORM\ManyToOne(targetEntity="Tracoda\ModeloBundle\Entity\CatalogoCategorias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_categoria", referencedColumnName="id")
     * })
     */
    private $idCategoria;

    /**
     * @var \Tracoda\ModeloBundle\Entity\SeguridadUsuario
     *
     * @ORM\ManyToOne(targetEntity="Tracoda\ModeloBundle\Entity\SeguridadUsuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $idUsuario;


    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Dataset
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set fechaSubida
     *
     * @param \DateTime $fechaSubida
     * @return Dataset
     */
    public function setFechaSubida($fechaSubida)
    {
        $this->fechaSubida = $fechaSubida;

        return $this;
    }

    /**
     * Get fechaSubida
     *
     * @return \DateTime
     */
    public function getFechaSubida()
    {
        return $this->fechaSubida;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Dataset
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fuenteDatos
     *
     * @param string $fuenteDatos
     * @return Dataset
     */
    public function setFuenteDatos($fuenteDatos)
    {
        $this->fuenteDatos = $fuenteDatos;

        return $this;
    }

    /**
     * Get fuenteDatos
     *
     * @return string
     */
    public function getFuenteDatos()
    {
        return $this->fuenteDatos;
    }

    /**
     * Set numVisitas
     *
     * @param integer $numVisitas
     * @return Dataset
     */
    public function setNumVisitas($numVisitas)
    {
        $this->numVisitas = $numVisitas;

        return $this;
    }

    /**
     * Get numVisitas
     *
     * @return integer
     */
    public function getNumVisitas()
    {
        return $this->numVisitas;
    }

    /**
     * Set publicado
     *
     * @param boolean $publicado
     * @return Dataset
     */
    public function setPublicado($publicado)
    {
        $this->publicado = $publicado;

        return $this;
    }

    /**
     * Get publicado
     *
     * @return boolean
     */
    public function getPublicado()
    {
        return $this->publicado;
    }

    /**
     * Get idDataset
     *
     * @return integer
     */
    public function getIdDataset()
    {
        return $this->idDataset;
    }

    /**
     * Set idLicencia
     *
     * @param \Tracoda\ModeloBundle\Entity\Licencia $idLicencia
     * @return Dataset
     */
    public function setIdLicencia(\Tracoda\ModeloBundle\Entity\Licencia $idLicencia = null)
    {
        $this->idLicencia = $idLicencia;

        return $this;
    }

    /**
     * Get idLicencia
     *
     * @return \Tracoda\ModeloBundle\Entity\Licencia
     */
    public function getIdLicencia()
    {
        return $this->idLicencia;
    }

    /**
     * Set idCategoria
     *
     * @param \Tracoda\ModeloBundle\Entity\CatalogoCategorias $idCategoria
     * @return Dataset
     */
    public function setIdCategoria(\Tracoda\ModeloBundle\Entity\CatalogoCategorias $idCategoria = null)
    {
        $this->idCategoria = $idCategoria;

        return $this;
    }

    /**
     * Get idCategoria
     *
     * @return \Tracoda\ModeloBundle\Entity\CatalogoCategorias
     */
    public function getIdCategoria()
    {
        return $this->idCategoria;
    }

    /**
     * Set idUsuario
     *
     * @param \Tracoda\ModeloBundle\Entity\SeguridadUsuario $idUsuario
     * @return Dataset
     */
    public function setIdUsuario(\Tracoda\ModeloBundle\Entity\SeguridadUsuario $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \Tracoda\ModeloBundle\Entity\SeguridadUsuario
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    public function __toString()
    {
        return $this->titulo;//getNombreCategoria();
    }
}
