<?php

namespace Tracoda\ModeloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Licencia
 *
 * @ORM\Table(name="licencia")
 * @ORM\Entity
 */
class Licencia
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="nombre", type="string", length=100, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", nullable=true)
     */
    private $url;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_licencia", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idLicencia;



    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Licencia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Licencia
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get idLicencia
     *
     * @return integer
     */
    public function getIdLicencia()
    {
        return $this->idLicencia;
    }

    public function __toString()
    {
        return $this->nombre;//getNombreCategoria();
    }
}
