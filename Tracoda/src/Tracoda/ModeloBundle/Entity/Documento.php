<?php

namespace Tracoda\ModeloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Documento
 *
 * @ORM\Table(name="Documento", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})}, indexes={@ORM\Index(name="id_dataset", columns={"id_dataset"}), @ORM\Index(name="id_usuario", columns={"id_usuario"})})
 * @ORM\Entity
 */
class Documento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_documento", type="string", length=255, nullable=false)
     */
    private $tituloDocumento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_subida", type="datetime", nullable=false)
     */
    private $fechaSubida;

    /**
     * @var string
     *
     * @ORM\Column(name="documento", type="string", length=255, nullable=false)
     */
    private $documento;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_visitas", type="integer", nullable=false)
     */
    private $numVisitas;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_descargas", type="integer", nullable=false)
     */
    private $numDescargas;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion_documento", type="string", length=255, nullable=false)
     */
    private $descripcionDocumento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=false)
     */
    private $fechaActualizacion;

    /**
     * @var \SeguridadUsuario
     *
     * @ORM\ManyToOne(targetEntity="SeguridadUsuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id")
     * })
     */
    private $idUsuario;

    /**
     * @var \Dataset
     *
     * @ORM\ManyToOne(targetEntity="Dataset")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dataset", referencedColumnName="id_dataset")
     * })
     */
    private $idDataset;

    /**
     * Set tituloDocumento
     *
     * @param string $tituloDocumento
     * @return Documento
     */
    public function setTituloDocumento($tituloDocumento)
    {
        $this->tituloDocumento = $tituloDocumento;

        return $this;
    }

    /**
     * Get tituloDocumento
     *
     * @return string 
     */
    public function getTituloDocumento()
    {
        return $this->tituloDocumento;
    }

    /**
     * Set descripcionDocumento
     *
     * @param string $descripcionDocumento
     * @return Documento
     */
    public function setDescripcionDocumento($descripcionDocumento)
    {
        $this->descripcionDocumento = $descripcionDocumento;

        return $this;
    }

    /**
     * Get descripcionDocumento
     *
     * @return string 
     */
    public function getDescripcionDocumento()
    {
        return $this->descripcionDocumento;
    }

    /**
     * Set fechaSubida
     *
     * @param \DateTime $fechaSubida
     * @return Documento
     */
    public function setFechaSubida($fechaSubida)
    {
        $this->fechaSubida = $fechaSubida;

        return $this;
    }

    /**
     * Get fechaSubida
     *
     * @return \DateTime 
     */
    public function getFechaSubida()
    {
        return $this->fechaSubida;
    }

    /**
     * Set fechaActualizacion
     *
     * @param \DateTime $fechaActualizacion
     * @return Documento
     */
    public function setFechaActualizacion($fechaActualizacion)
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    /**
     * Get fechaActualizacion
     *
     * @return \DateTime 
     */
    public function getFechaActualizacion()
    {
        return $this->fechaActualizacion;
    }

    /**
     * Set documento
     *
     * @param string $documento
     * @return Documento
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;

        return $this;
    }

    /**
     * Get documento
     *
     * @return string 
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * Set numVisitas
     *
     * @param integer $numVisitas
     * @return Documento
     */
    public function setNumVisitas($numVisitas)
    {
        $this->numVisitas = $numVisitas;

        return $this;
    }

    /**
     * Get numVisitas
     *
     * @return integer 
     */
    public function getNumVisitas()
    {
        return $this->numVisitas;
    }

    /**
     * Set numDescargas
     *
     * @param integer $numDescargas
     * @return Documento
     */
    public function setNumDescargas($numDescargas)
    {
        $this->numDescargas = $numDescargas;

        return $this;
    }

    /**
     * Get numDescargas
     *
     * @return integer 
     */
    public function getNumDescargas()
    {
        return $this->numDescargas;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idDataset
     *
     * @param \Tracoda\ModeloBundle\Entity\Dataset $idDataset
     * @return Documento
     */
    public function setIdDataset(\Tracoda\ModeloBundle\Entity\Dataset $idDataset = null)
    {
        $this->idDataset = $idDataset;

        return $this;
    }

    /**
     * Get idDataset
     *
     * @return \Tracoda\ModeloBundle\Entity\Dataset 
     */
    public function getIdDataset()
    {
        return $this->idDataset;
    }

    /**
     * Set idUsuario
     *
     * @param \Tracoda\ModeloBundle\Entity\SeguridadUsuario $idUsuario
     * @return Documento
     */
    public function setIdUsuario(\Tracoda\ModeloBundle\Entity\SeguridadUsuario $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \Tracoda\ModeloBundle\Entity\SeguridadUsuario 
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
}

