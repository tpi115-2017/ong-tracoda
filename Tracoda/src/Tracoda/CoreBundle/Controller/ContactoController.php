<?php

namespace Tracoda\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Tracoda\PlantillaBundle\Form\OpinionType;

class ContactoController extends Controller
{
  /* opinion función
* Objetivo: Ser la vista del formulario de envio de opiniones del sitio web
*/
  public function opinionAction(Request $request)
  {
      /*Crea el formulario*/
      $form = $this->createForm(new OpinionType());
      /*Variable utilizada para almacenar el estado del captcha*/
      $captchaVerify=true;

      if ($request->isMethod('POST')) {
          $form->handleRequest($request);
          /*Se ejecuta caso de exito si el formulario fue enviado, si es valido
          y si realizó el captcha*/
          if ($form->isSubmitted() &&  $form->isValid() && $this->captchaverify($request->get('g-recaptcha-response'))) {
              $mailer = $this->get('mailer');
              /*Se extraen el nombre,asunto,email y mensaje del formulario de opinion*/
              $message = $mailer->createMessage()
                  ->setSubject('TRACODA WEB: '. $form->get('asunto')->getData())
                  ->setFrom($form->get('email')->getData())
                  ->setTo('tpidevtracoda@gmail.com')
                  /*El cuerpo del email se establece en formato HTML*/
                  ->setBody(
                      $this->renderView(
                          'TracodaPlantillaBundle:Email:email.html.twig',
                          array(
                              'ip' => $request->getClientIp(),
                              'nombre' => $form->get('nombre')->getData(),
                              'email' => $form->get('email')->getData(),
                              'mensaje' => $form->get('mensaje')->getData()
                          )
                      )
                  );
                /*Envia el mensaje con la configuración establecida en config_dev/config_prod*/
              $mailer->send($message);
              /*Muestra un mensaje de exito */
              $request->getSession()->getFlashBag()->add('success', 'Tu email ha sido enviado. Gracias');
              return $this->redirect($this->generateUrl('tracoda_core_contacto_opinion'));
          }
          /*Captcha sin completar*/
          elseif ($form->isSubmitted() && !$this->captchaverify($request->get('g-recaptcha-response'))) {

            $captchaVerify=false;

          }



      }

      return $this->render('TracodaPlantillaBundle:Contacto:opinion.html.twig', array(
          'form'   => $form->createView(),
          'captchaverify'=>$captchaVerify
      ));
  }

  /* captchaverify función
  * Objetivo: obtener el resultado obtenido en el captcha y retornarlo
  */
    function captchaverify($recaptcha){
            $url = "https://www.google.com/recaptcha/api/siteverify";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array(
                "secret"=>"6LfTMzYUAAAAAN_Z8irbAd9Fra0kI_d7giUfxyPh","response"=>$recaptcha));
            $response = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($response);

        return $data->success;
    }
}
