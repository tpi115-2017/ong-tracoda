<?php

namespace Tracoda\CoreBundle\Controller;

use Tracoda\ModeloBundle\Entity\Documento;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Tracoda\PlantillaBundle\Form\DocumentoType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

/**
 * Documento controller.
 *
 * @Route("admin/documentos")
 */
class DocumentoController extends Controller
{
    /**
     * Lists all documento entities.
     *
     * @Route("/", name="admin_documentos_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $documentos = $em->getRepository('TracodaModeloBundle:Documento')->findAll();

        return $this->render('TracodaPlantillaBundle:documento:index.html.twig', array(
            'documentos' => $documentos,
        ));
    }

    /**
     * Creates a new documento entity.
     *
     * @Route("/new", name="admin_documentos_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $documento = new Documento();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new DocumentoType($em), $documento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $file stores the uploaded PDF file
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $documento->getDocumento();

            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            // Move the file to the directory where brochures are stored
            $file->move(
                #$documentoDir = $this->container->getparameter('kernel.root_dir').'/../web/subidas/categorias';
                $this->getparameter('kernel.root_dir').'/../web/subidas/docs',
                $fileName
            );

            // Update the 'brochure' property to store the PDF file name
            // instead of its contents
            $documento->setDocumento($fileName);
            
            $usr= $this->get('security.context')->getToken()->getUser();
            $documento->setIdUsuario($usr);

            $documento->setFechaSubida(new \DateTime('now'));
            $documento->setFechaActualizacion(new \DateTime('now'));
            $documento->setNumVisitas(0);
            $documento->setNumDescargas(0);


            $em->persist($documento);
            $em->flush();

            return $this->redirectToRoute('admin_documentos_show', array('id' => $documento->getId()));
        }

        return $this->render('TracodaPlantillaBundle:documento:new.html.twig', array(
            'documento' => $documento,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a documento entity.
     *
     * @Route("/{id}", name="admin_documentos_show")
     * @Method("GET")
     */
    public function showAction(Documento $documento)
    {
        $deleteForm = $this->createDeleteForm($documento);

        return $this->render('TracodaPlantillaBundle:documento:show.html.twig', array(
            'documento' => $documento,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing documento entity.
     *
     * @Route("/edit/{id}", name="admin_documentos_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Documento $documento)
    {
        $documento->setDocumento(new File ($this->get('kernel')->getRootDir().'/../web/subidas/docs/'.$documento->getDocumento()));
        $deleteForm = $this->createDeleteForm($documento);
        $em = $this->getDoctrine()->getManager();
        $editForm = $this->createForm(new DocumentoType($em), $documento);
        $editForm->handleRequest($request);
        $path=$this->getparameter('kernel.root_dir');

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $documento->setFechaActualizacion(new \DateTime('now'));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_documentos_show', array('id' => $documento->getId()));
        }

        return $this->render('TracodaPlantillaBundle:documento:edit.html.twig', array(
            'documento' => $documento,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'path'=>$path,
        ));
    }

    /**
     * Deletes a documento entity.
     *
     * @Route("/{id}", name="admin_documentos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Documento $documento)
    {
        $form = $this->createDeleteForm($documento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fileSystem = new Filesystem();
            /*Elimina el archivo del directorio*/
            $fileSystem->remove($this->get('kernel')->getRootDir().'/../web/subidas/docs/'.$documento->getDocumento());
            $em = $this->getDoctrine()->getManager();
            $em->remove($documento);
            $em->flush();
        }

        return $this->redirectToRoute('admin_documentos_index');
    }

    /**
     * Creates a form to delete a documento entity.
     *
     * @param Documento $documento The documento entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Documento $documento)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_documentos_delete', array('id' => $documento->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
