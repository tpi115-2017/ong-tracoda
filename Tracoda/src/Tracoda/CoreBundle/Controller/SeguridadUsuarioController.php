<?php

namespace Tracoda\CoreBundle\Controller;

use Tracoda\ModeloBundle\Entity\SeguridadUsuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Seguridadusuario controller.
 *
 * @Route("admin/usuarios")
 */
class SeguridadUsuarioController extends Controller
{
    /**
     * Lista todas las entidades de seguridadUsuario.
     *
     * @Route("/", name="admin_usuarios_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $seguridadUsuarios = $em->getRepository('TracodaModeloBundle:SeguridadUsuario')->findAll();

        return $this->render('TracodaPlantillaBundle:seguridadusuario:index.html.twig', array(
            'seguridadUsuarios' => $seguridadUsuarios,
        ));
    }

    /**
     * Crea una nueva entidad de seguridadUsuario.
     *
     * @Route("/new", name="admin_usuarios_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $seguridadUsuario = new Seguridadusuario();
        $form = $this->createForm('Tracoda\PlantillaBundle\Form\SeguridadUsuarioType', $seguridadUsuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /*Inicio Cifrado de contraseña*/
            $password = $this->get('security.password_encoder')->encodePassword($seguridadUsuario, $seguridadUsuario->getPassword());
            $seguridadUsuario->setPassword($password);
            /*Fin cifrado de contraseña*/
            $seguridadUsuario->setFechaCreacion(new \DateTime('now'));
            $seguridadUsuario->setFechaModificacion(new \DateTime('now'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($seguridadUsuario);
            $em->flush();

            return $this->redirectToRoute('admin_usuarios_show', array('id' => $seguridadUsuario->getId()));
        }

        return $this->render('TracodaPlantillaBundle:seguridadusuario:new.html.twig', array(
            'seguridadUsuario' => $seguridadUsuario,
            'form' => $form->createView(),
        ));
    }

    /**
     * Busca y despliega la informacion de una entidad seguridadUsuario
     *
     * @Route("/{id}", name="admin_usuarios_show")
     * @Method("GET")
     */
    public function showAction(SeguridadUsuario $seguridadUsuario)
    {
        $deleteForm = $this->createDeleteForm($seguridadUsuario);

        return $this->render('TracodaPlantillaBundle:seguridadusuario:show.html.twig', array(
            'seguridadUsuario' => $seguridadUsuario,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Despliega un formulario para editar una entidad existente de seguridadUsuario.
     *
     * @Route("/edit/{id}", name="admin_usuarios_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, SeguridadUsuario $seguridadUsuario)
    {
        $deleteForm = $this->createDeleteForm($seguridadUsuario);
        $editForm = $this->createForm('Tracoda\PlantillaBundle\Form\SeguridadUsuarioType', $seguridadUsuario);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            /*Inicio Cifrado de contraseña*/
            $password = $this->get('security.password_encoder')->encodePassword($seguridadUsuario, $seguridadUsuario->getPassword());
            $seguridadUsuario->setPassword($password);
            /*Fin cifrado de contraseña*/
            $seguridadUsuario->setFechaModificacion(new \DateTime('now'));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_usuarios_show', array('id' => $seguridadUsuario->getId()));
        }

        return $this->render('TracodaPlantillaBundle:seguridadusuario:edit.html.twig', array(
            'seguridadUsuario' => $seguridadUsuario,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Eliminar una entidad de seguridadUsuario.
     *
     * @Route("/delete/{id}", name="admin_usuario_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, SeguridadUsuario $seguridadUsuario)
    {
        $form = $this->createDeleteForm($seguridadUsuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($seguridadUsuario);
            $em->flush();
        }

        return $this->redirectToRoute('admin_usuarios_index');
    }

    /**
     * Crear un formulario para borrar una entidad de seguridadUsuario.
     *
     * @param SeguridadUsuario $seguridadUsuario The seguridadUsuario entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SeguridadUsuario $seguridadUsuario)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_usuario_delete', array('id' => $seguridadUsuario->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
