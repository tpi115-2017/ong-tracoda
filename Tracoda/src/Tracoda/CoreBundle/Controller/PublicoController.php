<?php

namespace Tracoda\CoreBundle\Controller;

use Tracoda\ModeloBundle\Entity\Pagina;
use Tracoda\ModeloBundle\Entity\Dataset;
use Tracoda\ModeloBundle\Entity\CatalogoCategorias;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Query\ResultSetMapping;
use Tracoda\PlantillaBundle\Form\ContactanosType;
use Tracoda\PlantillaBundle\Form\BusquedaDocumentoType;
use Symfony\Component\Finder\Finder;

class PublicoController extends Controller
{

  /**
  * Objetivo: Esta función es la encargada de gestionar el menú superior de navegación
  */
  public function menuAction(){
    /*Se obtienen las categorias existentes para ser enviadas al menú*/
    $em = $this->getDoctrine()->getManager();
    $catalogoCategorias = $em->getRepository('TracodaModeloBundle:CatalogoCategorias')->findAll();
    /*Se crea el formulario de busqueda*/
    $searchForm = $this->createForm(new BusquedaDocumentoType());

    return $this->render('TracodaPlantillaBundle:Publico:menu_superior.html.twig', array(
        'catalogoCategorias' => $catalogoCategorias,
        'searchForm' =>$searchForm->createView()
        ));
  }


  /**
  * Objetivo: Esta función es la encargada de mostrar los resultados de busqueda
  * @Route("/resultado-busqueda", name="tracoda_publico_resultado_busqueda")
  * @Method({"GET", "POST"})
  */
 public function resultadoBusquedaAction(Request $request)
   {

     $em = $this->getDoctrine()->getManager();
     $catalogoCategorias = $em->getRepository('TracodaModeloBundle:CatalogoCategorias')->findAll();
     if ($this->captchaverify($request->request->get('Captcha'))) {
          $documento = $request->request->get('documento');
          $titulo = $request->request->get('titulo');
          $indice = $request->request->get('indice');

          $doc = $em->getRepository('TracodaModeloBundle:Documento')->find($request->request->get('idDoc'));
          $doc->setNumDescargas($doc->getNumDescargas() + 1);
          $em->flush();

          return $this->render('TracodaPlantillaBundle:Publico:docLink.html.twig', array(
                'catalogoCategorias' => $catalogoCategorias,
                'documento' => $documento,
                'titulo' => $titulo,
                'indice' => $indice,
              ));
      } else{

          $em = $this->getDoctrine()->getManager();
          $documentos=null;
          $busco = null;
          $repository = $em->getRepository('TracodaModeloBundle:Documento');
          $searchForm = $this->createForm(new BusquedaDocumentoType());
          $searchForm->handleRequest($request);
          if ($request->isMethod('POST')) {
            if( $searchForm->isSubmitted() ) {
              /*Se extraen los datos del formulario de busqueda*/
              $busco = $searchForm->get('busco')->getData();
              $finder = new Finder();
              $finder->files()->in($this->getparameter('kernel.root_dir').'/../web/subidas/docs');
              $finder->contains($busco);
              $count=0;
              foreach ($finder as $file) {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('TracodaModeloBundle:Documento');
                $query = $repository->createQueryBuilder('d')
                 ->where('d.documento LIKE :nombre')
                 ->setParameter('nombre', '%'.$file->getRelativePathname().'%');
               $documentos[++$count] = $query->getQuery()->getSingleResult();
              }

            }
         }

         return $this->render('TracodaPlantillaBundle:Publico:busquedaDocumento.html.twig', array(
           'documentos'=>$documentos,
           'busco'=>$busco,
           'finder'=>$finder,
         ));
       }

     }

   /**
   * Objetivo: Esta función es la encargada de gestionar la pagina publica de inicio del sitio
   * @Route("/", name="tracoda_publico_inicio")
   * @Method({"GET", "POST"})
   */
  public function inicioAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $catalogoCategorias = $em->getRepository('TracodaModeloBundle:CatalogoCategorias')->findAll();
      return $this->render('TracodaPlantillaBundle:Publico:inicio.html.twig', array(
          'catalogoCategorias' => $catalogoCategorias
      ));
    }
    /*Fin de funcion: inicioAction*/

    /**
     * Objetivo: Esta función es la encargada de gestionar y mostrar los dataset's de la vista publica.
     * Fecha de modificación: 20/11/2017
     * @Route("/datasets/{id}", name="tracoda_publico_datasets")
     * @Method({"GET", "POST"})
     */
     public function datasetCategoriaAction(CatalogoCategorias $catalogoCategoria)
       {
         /*Se obtienen los datasets que pertenencen a una categoria*/
          $em = $this->getDoctrine()->getManager();
          $datasets = $em->getRepository('TracodaModeloBundle:Dataset')->findByIdCategoria($catalogoCategoria->getId());
           return $this->render('TracodaPlantillaBundle:Publico:datasets.html.twig', array(
               'datasets'=>$datasets,
               ));
       }
      /*Fin de funcion: datasetCategoriaAction*/

      /**
      * Objetivo: Esta función es la encargada de mostrar los documentos de un dataset
       * @Route("/dataset/{idDataset}", name="tracoda_publico_dataset")
     * @Method({"GET", "POST"})
       */
       public function documentosDatasetAction(Dataset $dataset,Request $request)
         {
           /* Si Se ha completado el captcha, se carga docLink.html que agrega el enlace de descarga */
           $em = $this->getDoctrine()->getManager();
           $catalogoCategorias = $em->getRepository('TracodaModeloBundle:CatalogoCategorias')->findAll();
           if ($this->captchaverify($request->request->get('Captcha'))) {
                $documento = $request->request->get('documento');
                $titulo = $request->request->get('titulo');
                $indice = $request->request->get('indice');

                $doc = $em->getRepository('TracodaModeloBundle:Documento')->find($request->request->get('idDoc'));
                $doc->setNumDescargas($doc->getNumDescargas() + 1);
                $em->flush();

                return $this->render('TracodaPlantillaBundle:Publico:docLink.html.twig', array(
                      'catalogoCategorias' => $catalogoCategorias,
                      'documento' => $documento,
                      'titulo' => $titulo,
                      'indice' => $indice,
                    ));
            }
            /* Si el captcha no es valido y el boton No se ha presionado, carga solamente index.html */
          else if(!$request->isMethod('POST')){

              $em = $this->getDoctrine()->getManager();
              $documentos = $em->getRepository('TracodaModeloBundle:Documento')->findByIdDataset($dataset->getIdDataSet());

              $dataset->setNumVisitas($dataset->getNumVisitas() + 1);
              $em->flush();

              return $this->render('TracodaPlantillaBundle:Publico:documentosDataset.html.twig', array(
                'documentos'=>$documentos,
              ));
            }
            /* Si presiona el boton pero el captcha no es valido, muestra errorDocLink.html */
            else {
              return $this->render('TracodaPlantillaBundle:Publico:errorDocLink.html.twig');
            }

         }
        /*Fin de Función: documentosDatasetAction*/

   /**
   * Objetivo: Esta función es la encargada de gestionar y mostrar la información de ¿Quiénes somos?
   * Fecha de modificación: 20/11/2017
   * @Route("/quienes-somos", name="tracoda_publico_quienes_somos")
   * @Method({"GET", "POST"})
   */
  public function quienesSomosAction()
    {
        $em = $this->getDoctrine()->getManager();
        /*Se busca una pagina donde la url contenga "quienes-somos" */
        $repository = $em->getRepository('TracodaModeloBundle:Pagina');
        $query = $repository->createQueryBuilder('p')
            ->where('p.url LIKE :url')
            ->setParameter('url','%quienes-somos%')
            ->getQuery();
        $paginas = $query->getResult();
        return $this->render('TracodaPlantillaBundle:Publico:quienesSomos.html.twig', array(
          'paginas'=>$paginas,
        ));
    }
    /*Fin de Función: quienesSomosAction*/

    /**
     * Objetivo: Esta función es la encargada de gestionar y mostrar la pagina de contactanos
     * @Route("/contactanos", name="tracoda_publico_contactanos")
     * @Method({"GET", "POST"})
     */
     public function contactanosAction(Request $request)
     {
         /*Crea el formulario*/
         $form = $this->createForm(new ContactanosType());
         /*Variable utilizada para almacenar el estado del captcha*/
         $captchaVerify=true;

         if ($request->isMethod('POST')) {
             $form->handleRequest($request);
             /*Se ejecuta caso de exito si el formulario fue enviado, si es valido
             y si realizó el captcha*/
             if ($form->isSubmitted() &&  $form->isValid() && $this->captchaverify($request->get('g-recaptcha-response'))) {
                 $mailer = $this->get('mailer');
                 /*Se extraen el nombre,asunto,email y mensaje del formulario de opinion*/

                 $message = $mailer->createMessage()
                     ->setSubject('TRACODA WEB: '. $form->get('asunto')->getData())
                     ->setFrom($form->get('email')->getData())
                     ->setTo('tpidevtracoda@gmail.com')
                     /*El cuerpo del email se establece en formato HTML*/
                     ->setBody(
                         $this->renderView(
                             'TracodaPlantillaBundle:Email:email.html.twig',
                             array(
                                 'ip' => $request->getClientIp(),
                                 'nombre' => $form->get('nombre')->getData(),
                                 'email' => $form->get('email')->getData(),
                                 'mensaje' => $form->get('mensaje')->getData(),
                             )
                         )
                     );
                   /*Envia el mensaje con la configuración establecida en config_dev/config_prod*/
                 $mailer->send($message);
                 /*Muestra un mensaje de exito */
                 $request->getSession()->getFlashBag()->add('success', 'Tu email ha sido enviado. Gracias');
                 return $this->redirect($this->generateUrl('tracoda_publico_contactanos'));
             }
             /*Captcha sin completar*/
             elseif ($form->isSubmitted() && !$this->captchaverify($request->get('g-recaptcha-response'))) {

               $captchaVerify=false;

             }

         }
         $em = $this->getDoctrine()->getManager();
         $catalogoCategorias = $em->getRepository('TracodaModeloBundle:CatalogoCategorias')->findAll();

         return $this->render('TracodaPlantillaBundle:Publico:contactanos.html.twig', array(
             'form'   => $form->createView(),
             'captchaverify'=>$captchaVerify,
         ));
     }
           /*Fin de Función: contactanosAction*/

     /*
     * Objetivo: Esta función es la encargada de retornar el estado de reCaptcha de google
     */
       private function captchaverify($recaptcha){
               $url = "https://www.google.com/recaptcha/api/siteverify";
               $ch = curl_init();
               curl_setopt($ch, CURLOPT_URL, $url);
               curl_setopt($ch, CURLOPT_HEADER, 0);
               curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
               curl_setopt($ch, CURLOPT_POST, true);
               curl_setopt($ch, CURLOPT_POSTFIELDS, array(
                    /*Secret es el código brindado por google*/
                   "secret"=>"6LfTMzYUAAAAAN_Z8irbAd9Fra0kI_d7giUfxyPh","response"=>$recaptcha));
               $response = curl_exec($ch);
               curl_close($ch);
               $data = json_decode($response);

           return $data->success;
       }
      /*Fin de Función: captchaverify*/

}
