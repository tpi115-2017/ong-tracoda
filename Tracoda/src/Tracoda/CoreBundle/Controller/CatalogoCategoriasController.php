<?php

namespace Tracoda\CoreBundle\Controller;

use Tracoda\ModeloBundle\Entity\CatalogoCategorias;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Tracoda\PlantillaBundle\Form\CatalogoCategoriasType;
use Symfony\Component\Filesystem\Filesystem;
use Tracoda\ModeloBundle\Entity\Dataset;
use Tracoda\ModeloBundle\Entity\Documento;
#use Symfony\Component\HttpFoundation\File\UploadedFile;
#use Symfony\Component\HttpFoundation\File\File;
/**
 * Catalogocategoria controller.
 *
 * @Route("admin/categorias")
 */
class CatalogoCategoriasController extends Controller
{
    /**
     * Lista todas las entidades de categorías.
     *
     * @Route("/", name="categorias_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $catalogoCategorias = $em->getRepository('TracodaModeloBundle:CatalogoCategorias')->findAll();

        return $this->render('TracodaPlantillaBundle:catalogocategorias:index.html.twig', array(
            'catalogoCategorias' => $catalogoCategorias,
        ));
    }
    /**
     * Crea un nuevo objeto de categoría
     *
     * @Route("/new", name="categorias_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $catalogoCategoria = new CatalogoCategorias();
        $form = $this->createForm('Tracoda\PlantillaBundle\Form\CatalogoCategoriasType', $catalogoCategoria);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $file almacena el archivo subido
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $catalogoCategoria->getImagenCategoria();

            // Se genera un nombre unico para el archivo subido
            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            // El archivo subido se almacena en la ruta /web/subidas/categorias
            $file->move(
                #$documentoDir = $this->container->getparameter('kernel.root_dir').'/../web/subidas/categorias';
                $this->getparameter('kernel.root_dir').'/../web/subidas/categorias',
                $fileName
            );

            // Update the 'brochure' property to store the PDF file name
            // instead of its contents
            $catalogoCategoria->setImagenCategoria($fileName);

            $em = $this->getDoctrine()->getManager();
            $em->persist($catalogoCategoria);
            $em->flush();

            return $this->redirectToRoute('categorias_show', array('id' => $catalogoCategoria->getId()));
        }
        return $this->render('TracodaPlantillaBundle:catalogocategorias:new.html.twig', array(
            'catalogoCategoria' => $catalogoCategoria,
            'form' => $form->createView(),
        ));
    }

    /**
     * Busca y despliega la información de una categoría
     *
     * @Route("/{id}", name="categorias_show")
     * @Method("GET")
     */
    public function showAction(CatalogoCategorias $catalogoCategoria)
    {
        $deleteForm = $this->createDeleteForm($catalogoCategoria);

        return $this->render('TracodaPlantillaBundle:catalogocategorias:show.html.twig', array(
            'catalogoCategoria' => $catalogoCategoria,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Presenta un formulario para editar una entidad de categoría.
     *
     * @Route("/edit/{id}", name="categorias_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, CatalogoCategorias $catalogoCategoria)
    {
        $deleteForm = $this->createDeleteForm($catalogoCategoria);
        $editForm = $this->createForm('Tracoda\PlantillaBundle\Form\CatalogoCategoriasType', $catalogoCategoria);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            // $file stores the uploaded PDF file
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $catalogoCategoria->getImagenCategoria();

            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            // Move the file to the directory where brochures are stored
            $file->move(
                #$documentoDir = $this->container->getparameter('kernel.root_dir').'/../web/subidas/categorias';
                $this->getparameter('kernel.root_dir').'/../web/subidas/categorias',
                $fileName
            );

            // Update the 'brochure' property to store the PDF file name
            // instead of its contents
            $catalogoCategoria->setImagenCategoria($fileName);

            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('categorias_show', array('id' => $catalogoCategoria->getId()));
        }
        /*$archivo = $catalogoCategoria->getImagenCategoria();
        if ($archivo != null)
        {//No funciona. Cuando quieren editar un registro, el archivo no se selecciona en el input type="file"
        //Y esta parte era para esto pero no funciona como se esperaba ya que no hace nada realmente
            $f = new File($this->getparameter('kernel.root_dir').'/../web/subidas/categorias/'.$archivo);
            $editForm->get('imagenCategoria')->setData($f);
        }*/

        return $this->render('TracodaPlantillaBundle:catalogocategorias:edit.html.twig', array(
            'catalogoCategoria' => $catalogoCategoria,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Eliminar una entidad de categoría.
     *
     * @Route("/delete/{id}", name="categorias_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, CatalogoCategorias $catalogoCategoria)
    {
        $form = $this->createDeleteForm($catalogoCategoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /*Elimina el archivo del directorio*/
            $fileSystem = new Filesystem();
            $fileSystem->remove($this->get('kernel')->getRootDir().'/../web/subidas/categorias/'.$catalogoCategoria->getImagenCategoria());

            $em = $this->getDoctrine()->getManager();

            $datasets = $em->getRepository('TracodaModeloBundle:Dataset')->findByIdCategoria($catalogoCategoria->getId());
            foreach ($datasets as $ds)
            {
                $documentos = $em->getRepository('TracodaModeloBundle:Documento')->findByIdDataset($ds->getIdDataset());
                foreach ($documentos as $doc)
                {
                  $documentoFile = new Filesystem();
                  $documentoFile->remove($this->get('kernel')->getRootDir().'/../web/subidas/docs/'.$doc->getDocumento());
                }
            }

            $em->remove($catalogoCategoria);
            $em->flush();
        }

        return $this->redirectToRoute('categorias_index');
    }

    /**
     * Crea un fomrulario para eliminar una entidad de categoría.
     *
     * @param CatalogoCategorias $catalogoCategoria The catalogoCategoria entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CatalogoCategorias $catalogoCategoria)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('categorias_delete', array('id' => $catalogoCategoria->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
