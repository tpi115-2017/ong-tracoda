<?php

namespace Tracoda\CoreBundle\Controller;

use Tracoda\ModeloBundle\Entity\Licencia;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Licencia controller.
 *
 * @Route("admin/licencias")
 */
class LicenciaController extends Controller
{
    /**
     * Lists all licencia entities.
     *
     * @Route("/", name="admin_licencias_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $licencias = $em->getRepository('TracodaModeloBundle:Licencia')->findAll();

        return $this->render('TracodaPlantillaBundle:Licencia:index.html.twig', array(
            'licencias' => $licencias,
        ));
    }

    /**
     * Creates a new licencia entity.
     *
     * @Route("/new", name="admin_licencias_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $licencia = new Licencia();
        $form = $this->createForm('Tracoda\PlantillaBundle\Form\LicenciaType', $licencia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($licencia);
            $em->flush();

            return $this->redirectToRoute('admin_licencias_show', array('idLicencia' => $licencia->getIdlicencia()));
        }

        return $this->render('TracodaPlantillaBundle:Licencia:new.html.twig', array(
            'licencia' => $licencia,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a licencia entity.
     *
     * @Route("/{idLicencia}", name="admin_licencias_show")
     * @Method("GET")
     */
    public function showAction(Licencia $licencia)
    {
        $deleteForm = $this->createDeleteForm($licencia);

        return $this->render('TracodaPlantillaBundle:Licencia:show.html.twig', array(
            'licencia' => $licencia,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing licencia entity.
     *
     * @Route("/edit/{idLicencia}", name="admin_licencias_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Licencia $licencia)
    {
        $deleteForm = $this->createDeleteForm($licencia);
        $editForm = $this->createForm('Tracoda\PlantillaBundle\Form\LicenciaType', $licencia);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_licencias_index', array('idLicencia' => $licencia->getIdlicencia()));
        }

        return $this->render('TracodaPlantillaBundle:Licencia:edit.html.twig', array(
            'licencia' => $licencia,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a licencia entity.
     *
     * @Route("/delete/{idLicencia}", name="admin_licencias_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Licencia $licencia)
    {
        $form = $this->createDeleteForm($licencia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($licencia);
            $em->flush();
        }

        return $this->redirectToRoute('admin_licencias_index');
    }

    /**
     * Creates a form to delete a licencia entity.
     *
     * @param Licencia $licencia The licencia entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Licencia $licencia)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_licencias_delete', array('idLicencia' => $licencia->getIdlicencia())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
