<?php

namespace Tracoda\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('TracodaCoreBundle:Default:index.html.twig');
    }
}
