<?php

namespace Tracoda\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

//Se carga la libreria de peticiones al servidor
use Symfony\Component\HttpFoundation\Request;

//Se carga la entidad a utilizar y el formulario de esa entidad
use Tracoda\ModeloBundle\Entity\Documento;
use Tracoda\PlantillaBundle\Form\DocumentoType;

class AdminController extends Controller
{
	/* Inicio función
	* Objetivo: Ser la vista principal panel administrativo del sitio web
	*/
	public function inicioAction()
    {
			/*Inicia la búsqueda de top 5 documentos más descargados*/
			$em = $this->getDoctrine()->getManager();
			$repository = $em->getRepository('TracodaModeloBundle:Documento');
			$query = $repository->createQueryBuilder('d')
				->select('d.tituloDocumento')
				->addSelect('d.numDescargas')
				->orderBy('d.numDescargas', 'DESC')
				->setMaxResults(5)
				->getQuery();
			$topDescargas = $query->getResult();
			/*Termina la busqueda de top 5 documentos más descargados*/

			/*Inicia la búsqueda de top 5 documentos más visitados*/
			$em = $this->getDoctrine()->getManager();
			$repository = $em->getRepository('TracodaModeloBundle:Documento');
			$query = $repository->createQueryBuilder('d')
				->select('d.tituloDocumento')
				->addSelect('d.numVisitas')
				->orderBy('d.numVisitas', 'DESC')
				->setMaxResults(5)
				->getQuery();
			$topVisitas = $query->getResult();
			/*Termina la búsqueda de top 5 documentos más visitados*/

			/*Se renderiza el panel administrativo junto con las consultas topDescargas y topVisitas*/
        return $this->render('TracodaPlantillaBundle:Administracion:admin.html.twig', array(
					'topDescargas'=>$topDescargas,
					'topVisitas'=>$topVisitas
				));
    }
    /*Fin de Función*/


    /* documento función
	* Objetivo: Ser la vista del formulario de subida de documentos del sitio web
	*/
    public function documentoAction(Request $request)
    {
    	$documento = new Documento();
    	$form = $this->createForm(DocumentoType::class, $documento);
    	$form->handleRequest($request);

    	if($request->isMethod('POST') && $form->isValid())
        {
            // La variable $file guardará el PDF subido
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $documento->getDocumento();

            // Generar un numbre único para el archivo antes de guardarlo
            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            // Mover el archivo al directorio donde se guardan los documentos
            $documentoDir = $this->container->getparameter('kernel.root_dir').'/../web/subidas/docs';
            $file->move($documentoDir, $fileName);

            // Actualizar la propiedad de los documentos para guardar el nombre de archivo PDF
            // en lugar de sus contenidos
            $documento->setDocumento($fileName);

            // ... persist la variable $documento o cualquier otra tarea
            $em = $this->getDoctrine()->getManager();
            $em->persist($documento);
            $em->flush();

            return $this->redirect($this->generateUrl('tracoda_core_inicio_admin'));
        }

    	return $this->render('TracodaPlantillaBundle:Administracion:documento.html.twig', array('form' => $form->createView(), ));

    }
    /*Fin de Función*/
}
