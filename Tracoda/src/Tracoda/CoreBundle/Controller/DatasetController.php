<?php

namespace Tracoda\CoreBundle\Controller;

use Tracoda\ModeloBundle\Entity\Dataset;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Tracoda\PlantillaBundle\Form\DatasetType;
use Symfony\Component\Filesystem\Filesystem;
use Tracoda\ModeloBundle\Entity\Documento;
/**
 * Dataset controller.
 *
 * @Route("admin/dataset")
 */
class DatasetController extends Controller
{
    /**
     * Lists all dataset entities.
     *
     * @Route("/", name="admin_dataset_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $datasets = $em->getRepository('TracodaModeloBundle:Dataset')->findAll();

        return $this->render('TracodaPlantillaBundle:dataset:index.html.twig', array(
            'datasets' => $datasets,
        ));
    }

    /**
     * Creates a new dataset entity.
     *
     * @Route("/new", name="dataset_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $dataset = new Dataset();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new DatasetType($em), $dataset);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $dataset->setFechaSubida(new \DateTime());
            $dataset->setNumVisitas(0);

            $usr= $this->get('security.context')->getToken()->getUser();
            $dataset->setIdUsuario($usr);

            $em->persist($dataset);
            $em->flush();

            return $this->redirectToRoute('dataset_show', array('idDataset' => $dataset->getIddataset()));
        }

        return $this->render('TracodaPlantillaBundle:dataset:new.html.twig', array(
            'dataset' => $dataset,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a dataset entity.
     *
     * @Route("/{idDataset}", name="dataset_show")
     * @Method("GET")
     */
    public function showAction(Dataset $dataset)
    {
        $deleteForm = $this->createDeleteForm($dataset);

        return $this->render('TracodaPlantillaBundle:dataset:show.html.twig', array(
            'dataset' => $dataset,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing dataset entity.
     *
     * @Route("/edit/{idDataset}", name="dataset_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Dataset $dataset)
    {
        $deleteForm = $this->createDeleteForm($dataset);
        $em = $this->getDoctrine()->getManager();
        $editForm = $this->createForm(new DatasetType($em), $dataset);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dataset_edit', array('idDataset' => $dataset->getIddataset()));
        }

        return $this->render('TracodaPlantillaBundle:dataset:edit.html.twig', array(
            'dataset' => $dataset,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a dataset entity.
     *
     * @Route("/{idDataset}", name="dataset_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Dataset $dataset)
    {
        $form = $this->createDeleteForm($dataset);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            //Eliminar los documentos asociados (archivos)
            $documentos = $em->getRepository('TracodaModeloBundle:Documento')->findByIdDataset($dataset->getIdDataset());
            foreach ($documentos as $doc)
            {
              $documentoFile = new Filesystem();
              $documentoFile->remove($this->get('kernel')->getRootDir().'/../web/subidas/docs/'.$doc->getDocumento());
            }

            $em->remove($dataset);
            $em->flush();
        }

        return $this->redirectToRoute('admin_dataset_index');
    }

    /**
     * Creates a form to delete a dataset entity.
     *
     * @param Dataset $dataset The dataset entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Dataset $dataset)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dataset_delete', array('idDataset' => $dataset->getIddataset())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
