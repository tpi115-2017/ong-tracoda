<?php

namespace Tracoda\CoreBundle\Handlers;

use Symfony\Component\HttpKernel\Bundle\Bundle;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    protected $router, $security;
    
    public function __construct(Router $router, SecurityContext $security)
    {
        $this->router = $router;
        $this->security = $security;
    }
    
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $url = 'tracoda_core_inicio_admin';
        /*if($this->security->isGranted('ROLE_ADMIN')) {
            $url = 'tracoda_core_inicio_admin';
        }
        elseif ($this->security->isGranted('ROLE_USER')) {
        	$url = 'tracoda_core_inicio_subida_documentos';
        }*/

        $response = new RedirectResponse($this->router->generate($url));
            
        return $response;
    }
}