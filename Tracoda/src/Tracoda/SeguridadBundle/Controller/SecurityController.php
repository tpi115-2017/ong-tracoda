<?php

namespace Tracoda\SeguridadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/*
* Nombre del módulo: Modulo de usuarios, Contraloría Social y Datos Abiertos.
* Dirección física: src/Tracoda/SeguridadBundle/Controller/SecurityController.php
* Objetivo: Controlador que permite realizar la  gestion de autenticacion de usuarios en las principales vistas del sitio web TRACODA.
**/

class SecurityController extends Controller
{
	/* Inicio función
	* Objetivo: Ser la vista principal del login del sitio web
	*/
	public function loginAction()
	{
		$authenticationUtils = $this->get('security.authentication_utils');

		$error = $authenticationUtils->getLastAuthenticationError();

		$lastUserName = $authenticationUtils->getLastUsername();

		return $this->render('TracodaPlantillaBundle:Seguridad:login.html.twig', array(
			'last_username' => $lastUserName, 
			'error' => $error)
		);
	}
	/*Fin de Función*/

	
	/* Inicio función
	* Objetivo: Ser una puente para la verificacion del login
	*/
	public function loginCheckAction()
	{
		
	}
	/*Fin de Función*/
}
